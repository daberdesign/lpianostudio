<?php
/**
 * L Piano Studio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package L_Piano_Studio
 */

if ( ! function_exists( 'lpianostudio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function lpianostudio_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on L Piano Studio, use a find and replace
		 * to change 'lpianostudio' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'lpianostudio', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'lpianostudio' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'lpianostudio_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
                        'default-repeat' => 'no-repeat',
                        'default-position-x' => 'left',
                        'default-attachment' => 'fixed',
                        'default-size' => 'cover'
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'lpianostudio_setup' );

/**
 * Register custom fonts.
 */
function lpianostudio_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Magra and Merriweather, translate this to 'off'. Do not translate
	 * into your own language.
	 */
        
	$magra = _x( 'on', 'Magra font: on or off', 'lpianostudio' );
        $merriweather = _x( 'on', 'Merriweather font: on or off', 'lpianostudio' );

        $font_families = array();
        
        if ( 'off' !== $magra ) {
            $font_families[] = 'Magra';
        }
        
        if ( 'off' !== $merriweather ) {
            $font_families[] = 'Merriweather:300,300i,400,400i,700,700i,900,900i';
        }
        
	if (in_array('on', array($magra,$merriweather)) ) {

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function lpianostudio_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'lpianostudio-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function lpianostudio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'lpianostudio_content_width', 640 );
}
add_action( 'after_setup_theme', 'lpianostudio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function lpianostudio_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'lpianostudio' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'lpianostudio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'lpianostudio_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function lpianostudio_scripts() {
        //Enqueue Google Fonts: Magra Bold for Banners and Merriweather 300 for captions, 400 for regular text, 700 for bold text, and 900 for titles or headers
        wp_enqueue_style( 'lpianostudio-fonts', lpianostudio_fonts_url() );
        
	wp_enqueue_style( 'lpianostudio-style', get_stylesheet_uri() );

	wp_enqueue_script( 'lpianostudio-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'lpianostudio-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'lpianostudio_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
