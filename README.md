# README #

lpianostudio is a WordPress theme created by www.daberdesign.com for www.lpianostudio.com

It is built from a SASSified version of underscores (http://underscores.me)

It will be utilizing css grid (https://gridbyexample.com/examples/)

There is an article on converting this theme to css grid here. (https://www.smashingmagazine.com/2017/06/building-production-ready-css-grid-layout/)

This is a test build being hosted on a local server.  It is not production-ready.